class CreateCheckUps < ActiveRecord::Migration[5.1]
  def change
    create_table :check_ups do |t|
      t.string :security_engineer_name
      t.string :account_manager_name
      t.string :partner_name
      t.string :original_file_name
      t.datetime :report_date
      t.string :file_uuid

      t.timestamps
    end
  end
end
