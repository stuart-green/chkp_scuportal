class CheckUpsController < ApplicationController
  
  def new
    @checkup = CheckUp.new()
  end
  
  
  def create
	
    @checkup = CheckUp.new(
    security_engineer_name: params[:check_up][:security_engineer_name],
    account_manager_name: params[:check_up][:account_manager_name],
    partner_name: params[:check_up][:partner_name],
    original_file_name: params[:check_up][:filename].original_filename,
    file_uuid: SecureRandom.uuid)
    
    @checkup.save
    if @checkup.errors.any?
      flash[:error] = @checkup.errors
      render 'new'
    else
	  uploaded_file = params[:check_up][:filename]
	  File.open(Rails.root.join('public', 'uploads', @checkup.file_uuid + File.extname(uploaded_file.original_filename)), 'wb') do |file|
	    file.write(uploaded_file.read)
	  end
      redirect_to home_path
    end
  end
  
end
