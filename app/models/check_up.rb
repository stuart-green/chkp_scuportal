class CheckUp < ApplicationRecord
  validates :security_engineer_name, presence: true
  validates :account_manager_name, presence: true
  validates :partner_name, presence: true
  validates :original_file_name, presence: true
  validates :file_uuid, presence: true 
end
