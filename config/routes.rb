Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resource :check_ups
  get '/', to: 'home#index', as: 'home'
  
end
